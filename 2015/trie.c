typedef struct Node {
  char key;
  int value;
  struct Node *sibling, *children;
} Node;

int isMember(Node *node, char keys[]) {
  Node *found;
  Node *location = node;
  while (location) {
  	if (location->key == keys[0]) {
  		found = location;
  		break;
  	}
  	location = location->sibling;
  }
  if (!(found)) return 0;
  if (keys[0] == '\n') return 1;
  return isMember(location->children, &keys[1]);
}


//add keys under the root
int add(Node *root, char keys[], int value) {
	if (keys[0] == '\0') {
		Node leaf = {0};
		leaf.key = keys[0];
		leaf.value = value;
		leaf.sibling = root->children;
		root->children = &leaf;
		return 1;
	}
	Node *temp;
	temp = root->children;
	while (temp) {
		if (keys[0] == temp->key) break;
		temp = temp->sibling;
	}
	if (!(temp)) {
		Node temp2 = {0};
		temp2.key = keys[0];
		temp2.sibling = root->children;
		root->children = &temp2;
		temp = &temp2;
	}
	return add(temp, &keys[1], value);
}

int main() {
  Node trie = {0};
  char thing1[] = "ab";
  add(&trie, thing1, 1);
  char thing2[] = "bb";
  add(&trie, thing2, 2);
  char thing3[] = "cb";
  add(&trie, thing3, 3);
  return 0;
}
