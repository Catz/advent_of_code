#include <stdio.h>
#include <string.h>

int main() {
  FILE *input;
  char action[255];
  int lights[1000][1000];
  int x_1, x_2, y_1, y_2, i, j;

  memset(lights, 0, sizeof(int) * 1000 * 1000);

  input = fopen("day_6_input.txt","r");

  while (fscanf(input, "%[^0123456789] %d,%d %*s %d,%d", action, &x_1, &y_1, &x_2, &y_2) == 5) {
    int string_length;
    string_length = strlen(action);
    for (i = x_1; i <= x_2; i++) {
      for (j = y_1; j <= y_2; j++) {
        switch (action[string_length - 2]) {
          case 'n':
            lights[i][j]++;
            break;
          case 'f':
            if (lights[i][j] > 0) lights[i][j]--;
            break;
          case 'e':
            lights[i][j] += 2;
        }
      }
    }
  }
  fclose(input);

  int temp;
  temp = 0;

  for (i = 0; i <= 999; i++) {
    for (j = 0; j <= 999; j++) {
      temp += lights[i][j];
    }
  }

  printf("Number of lights on: %d\n", temp);

  return 0;
}
