#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

int getSurfaceArea(int length, int width, int height) {
    int side_1 = length * width;
    int side_2 = width * height;
    int side_3 = height * length;
    int extera = 0;

    if (side_1 > side_3)
      std::swap(side_1, side_3);

    if (side_1 > side_2)
      std::swap(side_1, side_2);

    return (side_1 * 2) + (side_2 * 2) + (side_3 * 2) + side_1;
}

int getLength(int length, int width, int height) {
  int bow = length*width*height;

  if (length > width)
    std::swap(length, width);

  if (length > height)
    std::swap(length, height);

  if (height > width)
    std::swap(width, height);

  return bow + (2 * (length + height));
}

int main() {
  std::ifstream input;
  int total_area = 0;
  int total_length = 0;

  input.open("day_2_input.txt");
  std::string line;

  while (std::getline(input, line)) {
    std::stringstream stream(line);
    std::string temp;
    int length, width, height;
    std::getline(stream, temp, 'x');
    std::istringstream(temp) >> length;
    std::getline(stream, temp, 'x');
    std::istringstream(temp) >> width;
    std::getline(stream, temp, 'x');
    std::istringstream(temp) >> height;
    total_area += getSurfaceArea(length, width, height);
    total_length += getLength(length, width, height);
  }

  input.close();
  std::cout << "Total area of wrapping paper: " << total_area << std::endl;
  std::cout << "Total length of ribbon: " << total_length << std::endl;

}
