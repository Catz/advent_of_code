#include <iostream>
#include <fstream>

int main() {
  std::ifstream input;
  int level = 0;
  int position = 0;
  char character;
  input.open("day_1_input.txt");
  while (input >> character && level > -1) {
    switch (character) {
      case ')':
        level--;
        position++;
        break;
      case '(':
        level++;
        position++;
        break;
      default:
        break;
    }
  }
  if (level < 0){
    std::cout << "Entered the basment at position: " << position << std::endl;
  }
  std::cout << "Floor: " << level << std::endl;
  input.close();
  return 0;
}
