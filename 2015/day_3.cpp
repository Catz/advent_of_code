#include <iostream>
#include <fstream>
#include <set>

struct point {
  int x;
  int y;

  bool operator<(const point& rhs) const {
    if (x == rhs.x) {
      return y < rhs.y;
    } else {
      return x < rhs.x;
    }
  }
};

int main() {
  std::ifstream input;
  std::set<point> v;
  char movement;
  bool even = true;

  point real_santa, robot_santa;
  real_santa.x = 0;
  robot_santa.x = 0;
  real_santa.y = 0;
  robot_santa.y = 0;
  v.insert(real_santa);

  input.open("day_3_input.txt");

  while (input >> movement) {
    point santa = even ? real_santa : robot_santa;
    switch (movement) {
      case '^':
        santa.y++;
        break;
      case 'V':
      case 'v':
        santa.y--;
        break;
      case '<':
        santa.x--;
        break;
      case '>':
        santa.x++;
        break;
    }
    v.insert(santa);
    even = even ? false : true;
  }

  std::cout << "Number of unique houses: " << v.size() << std::endl;
  return 0;
}
