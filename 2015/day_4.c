#include <openssl/md5.h>
#include <math.h>
#include <stdio.h>

//-lm -crypto

int main() {
  const char secret_key[] = "yzbqklnj";
  const int secret_key_length = sizeof(secret_key) -1;

  int prefix = 0;
  unsigned char digest[MD5_DIGEST_LENGTH];

  while (1) {
    prefix++;
    int prefix_length = floor(log10(abs(prefix))) + 1;
    char string[prefix_length + secret_key_length];
    memset(string, 0, prefix_length + secret_key_length);
    memcpy(string, secret_key, secret_key_length);
    sprintf(&string[secret_key_length], "%d", prefix);
    MD5_CTX context;
    MD5_Init(&context);
    MD5_Update(&context, string, prefix_length + secret_key_length);
    MD5_Final(digest, &context);
    int temp = digest[0] + digest[1] + (digest[2] >> 4);
    if (digest[0] != 0) continue;
    if (digest[1] != 0) continue;
    if (digest[2] != 0 ) continue;
    printf("%d\n", prefix);
    break;
  }

  return 0;
}
