#include <stdio.h>

int isValidTriange(int side_1, int side_2, int side_3) {
  int buffer;

  if (side_1 > side_3) {
    buffer = side_3;
    side_3 = side_1;
    side_1 = buffer;
  }

  if (side_1 > side_2){
    buffer = side_2;
    side_2 = side_1;
    side_1 = buffer;
  }

  if (side_2 > side_3){
    buffer = side_3;
    side_3 = side_2;
    side_2 = buffer;
  }

  return (side_1 + side_2) > side_3;
}

int main() {
  int side_1;
  int side_2;
  int side_3;
  FILE* input;
  int validTriangles = 0;

  input = fopen("day_3_input.txt","r");

  while (fscanf(input, "%d %d %d\n", &side_1, &side_2, &side_3) == 3) {
    if (isValidTriange(side_1, side_2, side_3)) validTriangles++;
  }

  printf("%d\n", validTriangles);

  return 0;
}
