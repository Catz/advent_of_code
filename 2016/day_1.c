#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  int x;
  int y;
  int facing;
} Point;

Point* newPoint() {
  Point* temp;
  temp = (Point*) malloc(sizeof(Point));
  temp->x = 0;
  temp->y = 0;
  temp->facing = 1;
  return temp;
}

void turnLeft(Point* location) {
  switch (location->facing) {
    case 1: //north
      location->facing = 4;
      break;
    case 2: //south
      location->facing = 3;
      break;
    case 3: //east
      location->facing = 1;
      break;
    case 4: //west
      location->facing = 2;
      break;
  }
}

void turnRight(Point* location) {
  turnLeft(location);
  turnLeft(location);
  turnLeft(location);
}

void turn(Point* location, char* direction) {
  switch (*direction) {
    case 'R':
      turnRight(location);
      break;
    case 'L':
      turnLeft(location);
      break;
  }
}

void walk(Point* location, int distance) {
  switch (location->facing) {
    case 1: //north
      location->y += distance;
      break;
    case 2: //south
      location->y -= distance;
      break;
    case 3: //east
      location->x += distance;
      break;
    case 4: //west
      location->x -= distance;
      break;
  }
}

int main() {
  FILE *input;
  Point* location = newPoint();

  input = fopen("day_1_input.txt","r");

  char direction = 0;
  int distance = 0;

  while (fscanf(input, "%c%d, ", &direction, &distance) == 2) {
    turn(location, &direction);
    walk(location, distance);
  }

  printf("Distance: %d and %d\n", location->x, location->y);

  return 0;
}

typedef stuct {
  int color;
  int x;
  int y;
  Leaf* left;
  Leaf* right;
} Leaf;

typedef stuct {
  Leaf* root;
} RBTree;

RBTree* newRBTree() {
  RBTree* temp;
  temp = (RBTree*) malloc(sizeof(RBTree));
  temp->root = 0;
  return temp;
}

Leaf* newLeaf() {
  Leaf* temp;
  temp = (Leaf*) malloc(sizeof(Leaf));
  temp->x = 0;
  temp->y = 0;
  temp->left = 0;
  temp->right = 0;
  return temp;
}

int lessThan(Leaf* l1, Leaf* l2) {
  if (l1->x < l2->x) return 1;
  else if (l1->x == l2->x) {
    return l1->y < l2->y;
  }
  return 0;
}

int equalTo(Leaf* l1, Leaf* l2) {
  if (l1->x != l2->x) return 0;
  if (l1->y != l2->y) return 0;
  return 1;
}

void add(RBTree* tree, Leaf* leaf) {
  if (!tree->root) tree->root = leaf;
  if (lessThan(tree->root, leaf))
}
