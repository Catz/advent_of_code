#include <stdio.h>
#include <string.h>

int isValidTriange(int triangle[]) {
  int buffer;

  if (triangle[0] > triangle[2]) {
    buffer = triangle[2];
    triangle[2] = triangle[0];
    triangle[0] = buffer;
  }

  if (triangle[0] > triangle[1]){
    buffer = triangle[1];
    triangle[1] = triangle[0];
    triangle[0] = buffer;
  }

  if (triangle[1] > triangle[2]){
    buffer = triangle[2];
    triangle[2] = triangle[1];
    triangle[1] = buffer;
  }

  return (triangle[0] + triangle[1]) > triangle[2];
}

int main() {
  int triangle_1[3] = {0};
  int triangle_2[3] = {0};
  int triangle_3[3] = {0};
  FILE* input;
  int validTriangles = 0;
  int i = 0;
  char buffer[255];

  input = fopen("day_3_input.txt","r");

  while (fgets(buffer, sizeof(buffer), input)) {
    sscanf(buffer, "%d %d %d", &triangle_1[i], &triangle_2[i], &triangle_3[i]);
    if (i == 2) {
      if (isValidTriange(triangle_1)) validTriangles++;
      if (isValidTriange(triangle_2)) validTriangles++;
      if (isValidTriange(triangle_3)) validTriangles++;
    }
    i++;
    i %= 3;
  }

  printf("%d\n", validTriangles);

  return 0;
}
