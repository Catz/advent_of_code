#include <stdio.h>

enum {UP, RIGHT, DOWN, LEFT};

int getMovment(char temp) {
  switch (temp) {
    case 'U':
      return UP;
    case 'R':
      return RIGHT;
    case 'D':
      return DOWN;
    case 'L':
      return LEFT;
    default:
      return -1;
  }
}

int main() {
  int table_part_1[][4] = {{1,2,4,1}, {2,3,5,1}, {3,3,6,2}, {1,5,7,4}, {2,6,8,4}, {3,6,9,5}, {4,8,7,7}, {5,9,8,7}, {6,9,9,8}};
  int table_part_2[][4] = {{1,1,3,1}, {2,3,6,2}, {1,4,7,2}, {4,4,8,3}, {5,6,5,5}, {2,7,0xa,5}, {3,8,0xb,6}, {4,9,0xc,7}, {9,9,9,8}, {6,0xb,0xa,0xa}, {7,0xc,0xd,0xa}, {8,0xc,0xc,0xb}, {0xb, 0xd, 0xd, 0xd}};
  int location = 5;
  char c;
  FILE* input;
  input = fopen("day_2_input.txt","r");

  while ((c = fgetc(input)) != EOF) {
    if (c == '\n'){
      printf("%x", location);
    } else {
      location = table_part_2[location - 1][getMovment(c)];
    }
  }
  printf("\n");
  return 0;
}
